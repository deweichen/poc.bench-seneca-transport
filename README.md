# Seneca Transport Benchmarks
This project demonstrates speed of the various transport mechanisms in Seneca.

## Installation

Install dependencies:
```
$ npm i
```

Generate a self-signed SSL certificate for the HTTPS benchmark:
```
$ sudo openssl req -x509 -nodes -newkey rsa:2048 -keyout server.key -out server.crt
```

Configure connection settings properties in `config/default.json` to the appropriate address.

## Running benchmarks
To run the various benchmarking servers:
```
$ node index.js tcp-server
$ node index.js amqp-server
$ node index.js redis-server
$ node index.js mesh-server
```

To run the benchmarking clients:
```
$ node index.js tcp-bench
$ node index.js amqp-bench
$ node index.js redis-bench
$ node index.js mesh-bench
```

For help:
```
$ node index.js -h
```