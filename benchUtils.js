'use strict';

const Readable = require('stream').Readable;
const crypto = require('crypto');
const EventEmitter = require('events').EventEmitter;

// A readable stream that generates random bytes
class RandomReadableStream extends Readable {
  constructor(options) {
    super(options);
    this.totalBytes = options ? (options.totalBytes || 100) : 100;
    this.chunkSize = options ? (options.chunkSize || 100) : 100;
    // this.count = 0;
    this.countBytes = 0;
  }

  // _read(size) {
  //   let p = true;
  //   while (p && this.count < this.totalBytes) {
  //     let curSize = size;
  //     if (this.count + size > this.totalBytes) {
  //       curSize = this.totalBytes - this.count;
  //     }
  //     p = this.push(crypto.randomBytes(curSize));
  //     this.count += curSize;
  //     if (this.count === this.totalBytes) {
  //       this.push(null);
  //       p = false;
  //     }
  //   }
  // }

  _read() {
    if (this.countBytes + this.chunkSize >= this.totalBytes) {
      this.push(crypto.randomBytes(this.totalBytes - this.countBytes));
      this.countBytes += this.totalBytes - this.countBytes;
      // no more to read from stream
      this.push(null);
    } else {
      this.push(crypto.randomBytes(this.chunkSize));
      this.countBytes += this.chunkSize;
    }
  }
}

// A data emitter that generates random bytes with random chunk sizes at random intervals
class RandomBytesEmitter extends EventEmitter {
  constructor(options) {
    super();
    this.minChunkSize = options ? (options.minChunkSize || 1) : 1;
    this.maxChunkSize = options ? (options.maxChunkSize || 10) : 10;
    this.minIntervalLength = options ? (options.minIntervalLength || 1000) : 1000;
    this.maxIntervalLength = options ? (options.maxIntervalLength || 1000) : 1000;
    this.totalBytes = options ? (options.totalBytes || 10000) : 10000;
    this.byteCount = 0;
  }

  getRandomIntInclusive(min, max) {
    const intMin = Math.ceil(min);
    const intMax = Math.floor(max);
    return Math.floor(Math.random() * ((intMax - intMin) + 1)) + intMin;
  }

  _sendChunk(chunkSize) {
    const chunk = crypto.randomBytes(chunkSize);
    this.emit('chunk', chunk.toString());
    this.byteCount += chunkSize;
    if (this.byteCount >= this.totalBytes) {
      this.emit('end');
    }
  }

  start() {
    let timeCount = 0;
    let byteCount = 0;
    while (byteCount < this.totalBytes) {
      const randomIntervalLength = this.getRandomIntInclusive(this.minIntervalLength, this.maxIntervalLength);
      let randomChunkSize = this.getRandomIntInclusive(this.minChunkSize, this.maxChunkSize);
      if (byteCount + randomChunkSize > this.totalBytes) {
        randomChunkSize = this.totalBytes - byteCount;
      }
      timeCount += randomIntervalLength;
      byteCount += randomChunkSize;
      setTimeout(this._sendChunk.bind(this, randomChunkSize), timeCount);
    }
  }
}

module.exports = {
  RandomReadableStream,
  RandomBytesEmitter,
};
