'use strict';

const seneca = require('seneca');
const config = require('config');
const bunyan = require('bunyan');
const utils = require('./benchUtils');
const fs = require('fs');
const yargs = require('yargs');

const log = bunyan.createLogger(config.get('logger'));

// disable Node error caused by self-signed server SSL certificate
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

// do any clean up on CTRL-C exit
process.on('SIGINT', () => {
  log.info('Cleaning up');
  // TODO: do any clean up here
  process.exit(0);
});


const argv = yargs
  .usage('node $0 command [options]')
  .command('https-server', 'run https server')
  .command('https-bench', 'run https benchmarking client')
  .command('amqp-server', 'run amqp server')
  .command('amqp-bench', 'run amqp benchmarking client')
  .command('tcp-server', 'run tcp server')
  .command('tcp-bench', 'run tcp benchmarking client')
  .command('redis-server', 'run redis server')
  .command('redis-bench', 'run redis benchmarking client')
  .command('mesh-server', 'run mesh server')
  .command('mesh-bench', 'run mesh benchmarking client')
  .demandCommand(1, 1)
  .options({
    n: {
      alias: 'num',
      demandOption: true,
      default: 100,
      describe: 'total number of packets to send to the server',
      type: 'number',
    },
    s: {
      alias: 'size',
      demandOption: true,
      default: 100,
      describe: 'individual packet size in bytes',
      type: 'number',
    },
    p: {
      alias: 'port',
      describe: 'port number to run server on, works for TCP and HTTPS servers only',
      type: 'number',
    },
    t: {
      alias: 'targets',
      describe: 'array of target servers addresses (ipaddress:port), works for TCP and HTTPS bench clients only',
      type: 'array',
    },
    b: {
      alias: 'base',
      describe: 'base node for discovery by mesh',
      type: 'boolean',
    },
    v: {
      alias: 'verbose',
      describe: 'verbose console logs',
      type: 'boolean',
    },
  })
  .array('t')
  .help('h')
  .alias('h', 'help')
  .argv;

let benchClient = null;
let benchServer = null;
let requestCount = 0;

function serverResponse(req, done) {
  if (argv.v) {
    log.info(`Received request #${++requestCount}`);
  }
  return done(null, {
    ok: true,
    when: Date.now(),
  });
}

function benchRandomIntervalRandomData() {
  const dataEmitter = new utils.RandomBytesEmitter({
    minChunkSize: 50,
    maxChunkSize: 100,
    totalBytes: 1000,
    minIntervalLength: 100,
    maxIntervalLength: 1000,
  });

  dataEmitter.on('chunk', (chunk) => {
    benchClient.act('role:bench,type:rand_interval', { message: chunk }, (err, res) => {
      if (err) {
        throw err;
      }
      log.info(res);
    });
  });

  dataEmitter.on('end', () => {
    log.info('end of data');
    process.exit(0);
  });

  dataEmitter.start();
}

function benchLargeNumberOfMessages(messageSize, numMessages) {
  const dataStream = new utils.RandomReadableStream({
    totalBytes: messageSize * numMessages,
    chunkSize: messageSize,
  });
  const startTime = Date.now();
  let messageCount = 0;
  dataStream
    .on('data', (chunk) => {
      benchClient.act('role:bench,type:pkt_storm', { message: chunk.toString() }, (err, res) => {
        if (err) {
          return log.error(err);
        }
        if (argv.v) {
          log.info(res);
        }
        messageCount++;
        if (messageCount === numMessages) {
          log.info(`${messageCount} x ${messageSize}-byte messages sent`);
          log.info(`Time elapsed: ${Date.now() - startTime}ms`);
          return process.exit(0);
        }
        return null;
      });
    })
    .on('end', () => {
      if (argv.v) {
        log.info('End of data stream');
      }
    })
    .on('error', (err) => {
      log.fatal(err);
      process.exit(1);
    });
}

function runBenchmarks() {
  log.info('Running benchmarks...');
  benchLargeNumberOfMessages(argv.s, argv.n);
}

switch (argv._[0]) {
  case 'https-server':
    benchServer = seneca({ timeout: 99999 })
      .listen({
        host: config.get('https.host'),
        type: config.get('https.type'),
        port: argv.p || config.get('https.port'),
        protocol: config.get('https.protocol'),
        serverOptions: {
          key: fs.readFileSync(config.get('https.ssl.key'), 'utf8'),
          cert: fs.readFileSync(config.get('https.ssl.cert'), 'utf8'),
        },
      });
    break;
  case 'amqp-server':
    benchServer = seneca()
      .use('seneca-amqp-transport')
      .listen({
        type: 'amqp',
        pin: 'role:bench,type:*',
        url: config.get('amqp.url'),
      });
    break;
  case 'tcp-server':
    benchServer = seneca()
      .listen({
        host: config.get('tcp.host'),
        type: config.get('tcp.type'),
        port: argv.p || config.get('tcp.port'),
        protocol: config.get('tcp.protocol'),
      });
    break;
  case 'redis-server':
    benchServer = seneca()
      .use('seneca-redis-queue-transport')
      .listen({
        host: config.get('redis.host'),
        port: config.get('redis.port'),
        type: config.get('redis.type'),
        pin: 'role:bench,type:*',
      });
    break;
  case 'mesh-server':
    benchServer = seneca()
      .use('seneca-mesh', {
        isbase: argv.b || false,
        host: config.get('mesh.host'),
        port: argv.p || config.get('mesh.port'),
        listen: [
          { pin: 'role:bench,type:*', model: 'consume' },
        ],
      });
    break;
  case 'https-bench':
    benchClient = seneca({ timeout: 99999 })
      .use('seneca-balance-client')
      .client({
        host: config.get('https.host'),
        type: config.get('https.type'),
        port: config.get('https.port'),
        protocol: config.get('https.protocol'),
      });
    break;
  case 'amqp-bench':
    benchClient = seneca()
      .use('seneca-amqp-transport')
      .use('seneca-balance-client')
      .client({
        type: 'amqp',
        pin: 'role:bench,type:*',
        url: config.get('amqp.url'),
      });
    break;
  case 'tcp-bench':
    benchClient = seneca({ timeout: 99999 })
      .use('seneca-balance-client');
    if (argv.t) {
      // use client side load balancer for replicated servers
      benchClient = benchClient.client({ type: 'balance', model: 'consume' });
      argv.t.forEach((target) => {
        const parts = target.split(':');
        benchClient = benchClient.client({
          host: parts[0],
          port: Number.parseInt(parts[1], 10),
          type: config.get('tcp.type'),
          protocol: config.get('tcp.protocol'),
        });
      });
    } else {
      benchClient = benchClient.client({
        host: config.get('tcp.host'),
        type: config.get('tcp.type'),
        port: config.get('tcp.port'),
        protocol: config.get('tcp.protocol'),
      });
    }
    break;
  case 'redis-bench':
    benchClient = seneca()
      .use('seneca-redis-queue-transport')
      .use('seneca-balance-client')
      .client({
        host: config.get('redis.host'),
        port: config.get('redis.port'),
        type: config.get('redis.type'),
        pin: 'role:bench,type:*',
      });
    break;
  case 'mesh-bench':
    benchClient = seneca()
      .use('seneca-mesh');
    break;
  default:
    log.fatal('Unrecognized command');
    process.exit(1);
    break;
}

// Run the server
if (benchServer) {
  benchServer.ready((err) => {
    if (err) {
      log.fatal(err);
      process.exit(1);
    }
    benchServer.add('role:bench,type:*', serverResponse);
  });
} else if (benchClient) {
  // Start the benchmarks
  benchClient.ready((err) => {
    if (err) {
      log.fatal(err);
      process.exit(1);
    }
    runBenchmarks();
  });
}
